	$(document).ready(function(){
		var geocoder;
		var geocoder1;
	 	var map;
		var poly;

		function initialize(){
	    geocoder = new google.maps.Geocoder();
		geocoder1 = new google.maps.Geocoder();
	    var mapOptions = {
			disableDefaultUI: true,
			center:new google.maps.LatLng(-7.883107,110.333977),
	     	zoom: 13,
	     	mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
   		
   		map = new google.maps.Map(document.getElementById("map"), mapOptions);
   		var marker;
   		var infowindow = new google.maps.InfoWindow();

		$('.long').each(function(){
 			var latlng = ($(this).text()).split(',');
			marker = new google.maps.Marker({
		        position: new google.maps.LatLng(latlng[1],latlng[0]),
		        map: map
		    });
 		});

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
			codeLatLng(event.latLng);
		});
		
 		}

 		google.maps.event.addDomListener(window, 'load', initialize);
	});