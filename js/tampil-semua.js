	$(document).ready(function(){
		var geocoder;
		var geocoder1;
	 	var map;
		var poly;

		$('input[name="chkPos"]').click(function(){
			initialize();
		});
		
		function initialize(){
			var mapOptions = {
			zoom: 10,
			center: new google.maps.LatLng(-7.541359113905135, 110.44401168823242),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
		  
	    geocoder = new google.maps.Geocoder();
		geocoder1 = new google.maps.Geocoder();
	 
   		
   		map = new google.maps.Map(document.getElementById("map"), mapOptions);

   		var marker;
   		var infowindow = new google.maps.InfoWindow();

   		var posisi = {
		    url: 'images/pos.png',
		    size: new google.maps.Size(32, 32),
		    origin: new google.maps.Point(0,0),
		    anchor: new google.maps.Point(15, 32)
		  };

		if($('input[name="chkPos"]').is(':checked')){
	   		var i = 0;
			$('.longPos').each(function(){
	 			var latlng = ($(this).text()).split(',');
				marker = new google.maps.Marker({
			        position: new google.maps.LatLng(latlng[1],latlng[0]),
			        map: map,
			        icon: posisi
			    });

			    var content = $('.contentPos'+i).html();
				google.maps.event.addListener(marker, 'click', function(){
			      var marker = this;
			      var latLng = this.getPosition();
			      infowindow.setContent(content);
			      infowindow.open(map, marker);
				});
				i++;
	 		});
		}

 		}

 		google.maps.event.addDomListener(window, 'load', initialize);
	});