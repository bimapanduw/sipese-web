	var geocoder;
	var geocoder1;
 	var map;
	var poly;
	function initialize(){

    geocoder = new google.maps.Geocoder();
	geocoder1 = new google.maps.Geocoder();
    var mapOptions = {
		disableDefaultUI: true,
		center:new google.maps.LatLng(-7.541359113905135, 110.44401168823242),
     	zoom: 10,
     	mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
		
		var marker;
		var madrasah = {
		    url: 'images/madrasah.png',
		    size: new google.maps.Size(32, 32),
		    origin: new google.maps.Point(0,0),
		    anchor: new google.maps.Point(15, 32)
		  };
		function placeMarker(location) {
		  if ( marker ) {
			marker.setPosition(location);
			$('input[name="txtLongitude"]').val(location.lng());
			$('input[name="txtLatitude"]').val(location.lat());
		  } else {
			marker = new google.maps.Marker({
			  position: location,
			  map: map,
			  icon: madrasah
			});
			$('input[name="txtLongitude"]').val(location.lng());
			$('input[name="txtLatitude"]').val(location.lat());
		  }
		}  
		
		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
			codeLatLng(event.latLng);
		});
		
		function codeLatLng(latlng1){
			geocoder1.geocode( { 'latLng': latlng1}, function(results, status){
				if(status == google.maps.GeocoderStatus.OK) {
				  for (var i = 0; i < results[0].address_components.length; i++){
					  var addr = results[0].address_components[i];
					  if(addr.types[0] == 'route'){
						  var getJalan = addr.long_name;
					  }
				  }
				  //$('input[name="txtNama"]').val(getJalan);
				}
			});
		}
  }
  
  // function codeAddress() {
  //   var address = document.getElementById("address").value;
  //   geocoder.geocode( { 'address': address}, function(results, status) {
  //     if (status == google.maps.GeocoderStatus.OK) {
  //       map.setCenter(results[0].geometry.location);
		// map.setZoom(15);
		
	 //  for (var i = 0; i < results[0].address_components.length; i++){
  //       var addr = results[0].address_components[i];
  //       var getPostal;
		// var getKecamatan;
		// var getDesa;
		// 	if(addr.types[0] == 'postal_code'){
		// 	  getPostal = addr.long_name;
		// 	}else if(addr.types[0] == 'locality'){
		// 	  getKecamatan = addr.long_name;
		// 	}else if(addr.types[0] == 'sublocality'){
		// 	  getDesa = addr.long_name;
		// 	}
  //     	}
		// $('input[name="txtKode"]').val(getPostal);
		// $('input[name="txtKec"]').val(getKecamatan);
		// $('input[name="txtDesa"]').val(getDesa);
  //     } else {
  //       alert("Geocode was not successful for the following reason: " + status);
  //     }
  //   });
  // }
  
  google.maps.event.addDomListener(window, 'load', initialize);