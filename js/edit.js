	var geocoder;
	var geocoder1;
 	var map;
	var poly;
	function initialize(){

    geocoder = new google.maps.Geocoder();
	geocoder1 = new google.maps.Geocoder();
    var mapOptions = {
		disableDefaultUI: true,
		center:new google.maps.LatLng(-7.541359113905135, 110.44401168823242),
     	zoom: 10,
     	mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
		
		var marker;
		var lat1 = document.getElementById("lat");
		var long1 = document.getElementById("long");
		var markLatLng = new google.maps.LatLng(lat1.value, long1.value);
		marker = new google.maps.Marker({
			position: markLatLng,
			map: map
		});

		function placeMarker(location) {
		  if ( marker ) {
			marker.setPosition(location);
			$('input[name="txtLongitude"]').val(location.lng());
			$('input[name="txtLatitude"]').val(location.lat());
		  } else {
			marker = new google.maps.Marker({
			  position: location,
			  map: map
			});
			$('input[name="txtLongitude"]').val(location.lng());
			$('input[name="txtLatitude"]').val(location.lat());
		  }
		}  
		
		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
		});
		
  }
  
  google.maps.event.addDomListener(window, 'load', initialize);