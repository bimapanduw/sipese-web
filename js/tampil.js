	$(document).ready(function(){
		var geocoder;
		var geocoder1;
	 	var map;
		var poly;

		function initialize(){
			var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(-7.883107, 110.333977),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
		  
	    geocoder = new google.maps.Geocoder();
		geocoder1 = new google.maps.Geocoder();
   		
   		map = new google.maps.Map(document.getElementById("map"), mapOptions);
   		var marker;
   		var infowindow = new google.maps.InfoWindow();
   		var i = 0;
		$('.long').each(function(){
 			var latlng = ($(this).text()).split(',');
			marker = new google.maps.Marker({
		        position: new google.maps.LatLng(latlng[1],latlng[0]),
		        map: map
		    });

		    var content = $('.content'+i).html();
			google.maps.event.addListener(marker, 'click', function(){
		      var marker = this;
		      var latLng = this.getPosition();
		      infowindow.setContent(content);
		      infowindow.open(map, marker);
			});
			i++;
 		});

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
			codeLatLng(event.latLng);
		});
		
 		}

 		google.maps.event.addDomListener(window, 'load', initialize);
	});