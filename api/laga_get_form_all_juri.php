<?php
// get_all_products.php
 
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();
 
// get all products from products table
$result = mysql_query("SELECT * FROM partai") or die(mysql_error());
 
// check for empty row
if (mysql_num_rows($result) > 0) {
    // looping through all rows
    // products node
    $response["form"] = array();
 
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $form = array();
        
		$form["juri"] = $row["id"];
		$form["merah1"] = $row["merahI"];
		$form["merah2"] = $row["merahII"];
		$form["merah3"] = $row["merahIII"];
		$form["merahtot"] = $row["merahtot"];
		
		$form["biru1"] = $row["biruI"];
		$form["biru2"] = $row["biruII"];
		$form["biru3"] = $row["biruIII"];
		$form["birutot"] = $row["birutot"];
		
		$form["totmerah1"] = $row["totmerahI"];
		$form["totmerah2"] = $row["totmerahII"];
		$form["totmerah3"] = $row["totmerahIII"];
		
		$form["totbiru1"] = $row["totbiruI"];
		$form["totbiru2"] = $row["totbiruII"];
		$form["totbiru3"] = $row["totbiruIII"];
		
		$form["pemenang"] = $row["pemenang"];
		$form["menang_karena"] = $row["menang_karena"];
		
		$form["nilai_hukuman1m"] = $row["nilai_hukuman1m"];
		$form["nilai_hukuman2m"] = $row["nilai_hukuman2m"];
		$form["nilai_hukuman3m"] = $row["nilai_hukuman3m"];
		$form["total_hukumanm"] = $row["total_hukumanm"];
		
		$form["nilai_hukuman1b"] = $row["nilai_hukuman1b"];
		$form["nilai_hukuman2b"] = $row["nilai_hukuman2b"];
		$form["nilai_hukuman3b"] = $row["nilai_hukuman3b"];
		$form["total_hukumanb"] = $row["total_hukumanb"];
		
		$form["nilai_plus1m"] = $row["nilai_plus1m"];
		$form["nilai_plus2m"] = $row["nilai_plus2m"];
		$form["nilai_plus3m"] = $row["nilai_plus3m"];
		$form["total_plusm"] = $row["total_plusm"];
		
		$form["nilai_plus1b"] = $row["nilai_plus1b"];
		$form["nilai_plus2b"] = $row["nilai_plus2b"];
		$form["nilai_plus3b"] = $row["nilai_plus3b"];
		$form["total_plusb"] = $row["total_plusb"];
		
		$form["nilai_plusA1m"] = $row["nilai_plusA1m"];
		$form["nilai_plusA2m"] = $row["nilai_plusA2m"];
		$form["nilai_plusA3m"] = $row["nilai_plusA3m"];
		
		$form["nilai_plusA1b"] = $row["nilai_plusA1b"];
		$form["nilai_plusA2b"] = $row["nilai_plusA2b"];
		$form["nilai_plusA3b"] = $row["nilai_plusA3b"];
		
		$form["nilai_plusB1m"] = $row["nilai_plusB1m"];
		$form["nilai_plusB2m"] = $row["nilai_plusB2m"];
		$form["nilai_plusB3m"] = $row["nilai_plusB3m"];
		
		$form["nilai_plusB1b"] = $row["nilai_plusB1b"];
		$form["nilai_plusB2b"] = $row["nilai_plusB2b"];
		$form["nilai_plusB3b"] = $row["nilai_plusB3b"];
 
        // push single product into final response array
        array_push($response["form"], $form);
    }
    // success
    $response["success"] = 1;
 
    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No Form found";
 
    // echo no users JSON
    echo json_encode($response);
}
?>