<?php
// get_product_details.php
  
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();
 
// check for post data
if (isset($_GET["juri"])) {
    $juri = $_GET["juri"];
 
    // get a product from products table
    $result = mysql_query("SELECT * FROM partai WHERE id = $juri");
 
    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {
 
            $result = mysql_fetch_array($result);
 
            $form = array();
            $form["juri"] = $result["id"];
            $form["merah1"] = $result["merahI"];
            $form["merah2"] = $result["merahII"];
            $form["merah3"] = $result["merahIII"];
            $form["merahtot"] = $result["merahtot"];
			
            $form["biru1"] = $result["biruI"];
            $form["biru2"] = $result["biruII"];
            $form["biru3"] = $result["biruIII"];
            $form["birutot"] = $result["birutot"];
			
            $form["totmerah1"] = $result["totmerahI"];
            $form["totmerah2"] = $result["totmerahII"];
            $form["totmerah3"] = $result["totmerahIII"];
			
            $form["totbiru1"] = $result["totbiruI"];
            $form["totbiru2"] = $result["totbiruII"];
            $form["totbiru3"] = $result["totbiruIII"];
			
            $form["pemenang"] = $result["pemenang"];
            $form["menang_karena"] = $result["menang_karena"];
			
            $form["nilai_hukuman1m"] = $result["nilai_hukuman1m"];
            $form["nilai_hukuman2m"] = $result["nilai_hukuman2m"];
            $form["nilai_hukuman3m"] = $result["nilai_hukuman3m"];
            $form["total_hukumanm"] = $result["total_hukumanm"];
			
			$form["nilai_hukuman1b"] = $result["nilai_hukuman1b"];
            $form["nilai_hukuman2b"] = $result["nilai_hukuman2b"];
            $form["nilai_hukuman3b"] = $result["nilai_hukuman3b"];
            $form["total_hukumanb"] = $result["total_hukumanb"];
			
			$form["nilai_plus1m"] = $result["nilai_plus1m"];
			$form["nilai_plus2m"] = $result["nilai_plus2m"];
			$form["nilai_plus3m"] = $result["nilai_plus3m"];
            $form["total_plusm"] = $result["total_plusm"];
			
			$form["nilai_plus1b"] = $result["nilai_plus1b"];
			$form["nilai_plus2b"] = $result["nilai_plus2b"];
			$form["nilai_plus3b"] = $result["nilai_plus3b"];
            $form["total_plusb"] = $result["total_plusb"];
			
			$form["nilai_plusA1m"] = $result["nilai_plusA1m"];
			$form["nilai_plusA2m"] = $result["nilai_plusA2m"];
			$form["nilai_plusA3m"] = $result["nilai_plusA3m"];
			
			$form["nilai_plusA1b"] = $result["nilai_plusA1b"];
			$form["nilai_plusA2b"] = $result["nilai_plusA2b"];
			$form["nilai_plusA3b"] = $result["nilai_plusA3b"];
			
			$form["nilai_plusB1m"] = $result["nilai_plusB1m"];
			$form["nilai_plusB2m"] = $result["nilai_plusB2m"];
			$form["nilai_plusB3m"] = $result["nilai_plusB3m"];
			
			$form["nilai_plusB1b"] = $result["nilai_plusB1b"];
			$form["nilai_plusB2b"] = $result["nilai_plusB2b"];
			$form["nilai_plusB3b"] = $result["nilai_plusB3b"];
			
            // success
            $response["success"] = 1;
 
            // user node
            $response["form"] = array();
 
            array_push($response["form"], $form);
 
            // echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No Form found";
 
            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No Form found";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>