<?php
// update_product.php
 
/*
 * Following code will update a product information
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['id'])) {
 
    $juri = $_GET["id"];
	$merah1 = isset($_GET['merahI'])?",merahI = '".$_GET['merahI']."'":"";
	$merah2 = isset($_GET["merahII"])?",merahII = '".$_GET['merahII']."'":"";
	$merah3 = isset($_GET["merahIII"])?",merahIII = '".$_GET['merahIII']."'":"";
	$merahtot = isset($_GET["merahtot"])?",merahtot = '".$_GET['merahtot']."'":"";
	
	$biru1 = isset($_GET["biruI"])?",biruI = '".$_GET['biruI']."'":"";
	$biru2 = isset($_GET["biruII"])?",biruII = '".$_GET['biruII']."'":"";
	$biru3 = isset($_GET["biruIII"])?",biruIII = '".$_GET['biruIII']."'":"";
	$birutot = isset($_GET["birutot"])?",birutot = '".$_GET['birutot']."'":"";
	
	$totmerah1 = isset($_GET["totmerahI"])?",totmerahI = '".$_GET['totmerahI']."'":"";
	$totmerah2 = isset($_GET["totmerahII"])?",totmerahII = '".$_GET['totmerahII']."'":"";
	$totmerah3 = isset($_GET["totmerahIII"])?",totmerahIII = '".$_GET['totmerahIII']."'":"";
	
	$totbiru1 = isset($_GET["totbiruI"])?",totbiruI = '".$_GET['totbiruI']."'":"";
	$totbiru2 = isset($_GET["totbiruII"])?",totbiruII = '".$_GET['totbiruII']."'":"";
	$totbiru3 = isset($_GET["totbiruIII"])?",totbiruIII = '".$_GET['totbiruIII']."'":"";
	
	$pemenang = isset($_GET["pemenang"])?",pemenang = '".$_GET['pemenang']."'":"";
	$menang_karena = isset($_GET["menang_karena"])?",menang_karena = '".$_GET['menang_karena']."'":"";
	
	$nilai_hukuman1m = isset($_GET["nilai_hukuman1m"])?",nilai_hukuman1m = '".$_GET['nilai_hukuman1m']."'":"";
	$nilai_hukuman2m = isset($_GET["nilai_hukuman2m"])?",nilai_hukuman2m = '".$_GET['nilai_hukuman2m']."'":"";
	$nilai_hukuman3m = isset($_GET["nilai_hukuman3m"])?",nilai_hukuman3m = '".$_GET['nilai_hukuman3m']."'":"";
	$total_hukumanm = isset($_GET["total_hukumanm"])?",total_hukumanm = '".$_GET['total_hukumanm']."'":"";
	
	$nilai_hukuman1b = isset($_GET["nilai_hukuman1b"])?",nilai_hukuman1b = '".$_GET['nilai_hukuman1b']."'":"";
	$nilai_hukuman2b = isset($_GET["nilai_hukuman2b"])?",nilai_hukuman2b = '".$_GET['nilai_hukuman2b']."'":"";
	$nilai_hukuman3b = isset($_GET["nilai_hukuman3b"])?",nilai_hukuman3b = '".$_GET['nilai_hukuman3b']."'":"";
	$total_hukumanb = isset($_GET["total_hukumanb"])?",total_hukumanb = '".$_GET['total_hukumanb']."'":"";
	
	$nilai_plus1m = isset($_GET["nilai_plus1m"])?",nilai_plus1m = '".$_GET['nilai_plus1m']."'":"";
	$nilai_plus2m = isset($_GET["nilai_plus2m"])?",nilai_plus2m = '".$_GET['nilai_plus2m']."'":"";
	$nilai_plus3m = isset($_GET["nilai_plus3m"])?",nilai_plus3m = '".$_GET['nilai_plus3m']."'":"";
	$total_plusm = isset($_GET["total_plusm"])?",total_plusb = '".$_GET['total_plusm']."'":"";
	
	$nilai_plus1b = isset($_GET["nilai_plus1b"])?",nilai_plus1b = '".$_GET['nilai_plus1b']."'":"";
	$nilai_plus2b = isset($_GET["nilai_plus2b"])?",nilai_plus2b = '".$_GET['nilai_plus2b']."'":"";
	$nilai_plus3b = isset($_GET["nilai_plus3b"])?",nilai_plus3b = '".$_GET['nilai_plus3b']."'":"";
	$total_plusb = isset($_GET["total_plusb"])?",total_plusb = '".$_GET['total_plusb']."'":"";
	
	$nilai_plusA1m = isset($_GET["nilai_plusA1m"])?",nilai_plusA1m = '".$_GET['nilai_plusA1m']."'":"";
	$nilai_plusA2m = isset($_GET["nilai_plusA2m"])?",nilai_plusA2m = '".$_GET['nilai_plusA2m']."'":"";
	$nilai_plusA3m = isset($_GET["nilai_plusA3m"])?",nilai_plusA3m = '".$_GET['nilai_plusA3m']."'":"";
	
	$nilai_plusA1b = isset($_GET["nilai_plusA1b"])?",nilai_plusA1b = '".$_GET['nilai_plusA1b']."'":"";
	$nilai_plusA2b = isset($_GET["nilai_plusA2b"])?",nilai_plusA2b = '".$_GET['nilai_plusA2b']."'":"";
	$nilai_plusA3b = isset($_GET["nilai_plusA3b"])?",nilai_plusA3b = '".$_GET['nilai_plusA3b']."'":"";
	
	$nilai_plusB1m = isset($_GET["nilai_plusB1m"])?",nilai_plusB1m = '".$_GET['nilai_plusB1m']."'":"";
	$nilai_plusB2m = isset($_GET["nilai_plusB2m"])?",nilai_plusB2m = '".$_GET['nilai_plusB2m']."'":"";
	$nilai_plusB3m = isset($_GET["nilai_plusB3m"])?",nilai_plusB3m = '".$_GET['nilai_plusB3m']."'":"";
	
	$nilai_plusB1b = isset($_GET["nilai_plusB1b"])?",nilai_plusB1b = '".$_GET['nilai_plusB1b']."'":"";
	$nilai_plusB2b = isset($_GET["nilai_plusB2b"])?",nilai_plusB2b = '".$_GET['nilai_plusB2b']."'":"";
	$nilai_plusB3b = isset($_GET["nilai_plusB3b"])?",nilai_plusB3b = '".$_GET['nilai_plusB3b']."'":"";
 
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
    // mysql update _GET with matched pid
    $result = mysql_query("UPDATE partai SET id = '$juri'
							$merah1 $merah2 $merah3 $merahtot $biru1 $biru2 $biru3 $birutot $totmerah1 $totmerah2 
							$totmerah3 $totbiru1 $totbiru2 $totbiru3 $pemenang $menang_karena
							WHERE id = $juri");
 
    // check if _GET inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Form successfully updated.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
 
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>