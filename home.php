<?php 
	session_start();

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Halaman Admin</title>
<?php
	include "config/koneksi.php";
?>
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="js/jquery-ui.css" />
  <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
      <script>
  $(function() {
    $( ".date" ).datepicker();
  });
  </script>
<!-- <script type="text/javascript" src="js/jquery-1.4.2.js"></script> -->

<link href="css/styleadmin.css" rel="stylesheet" type="text/css" />
<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDELMKvg8rQbocDjCc0k29eVYqNZ9vXZ3k&sensor=false&libraries=drawing"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDELMKvg8rQbocDjCc0k29eVYqNZ9vXZ3k&sensor=false&libraries=drawing"></script>
</head>
<body>
	<div id="header"> 
		<center><img src="images/headeradmin.png" width="1244" height="200"/>  </center>
	</div>		
	
	<div id="wp"> 
		<div id="menu">
			<?php 
				include "menu.php";
			 ?> 
		</div>
	</div>
</div>
<div id="wrap">
	
    <div id="content">
    <?php
		include "content.php";
                ?>
    </div>
</div>
<!--<div id="footer"></div>-->
</body>
</html>