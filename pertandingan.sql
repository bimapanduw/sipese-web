-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2015 at 07:50 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pertandingan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `cache_pool`
--

CREATE TABLE IF NOT EXISTS `cache_pool` (
  `pool` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cache_pool`
--

INSERT INTO `cache_pool` (`pool`) VALUES
('final');

-- --------------------------------------------------------

--
-- Table structure for table `form_ganda`
--

CREATE TABLE IF NOT EXISTS `form_ganda` (
  `juri` int(1) NOT NULL,
  `senjata_lepas` int(2) NOT NULL,
  `pakaian` int(3) NOT NULL,
  `suara` int(3) NOT NULL,
  `garis` int(3) NOT NULL,
  `serang_bela` int(11) NOT NULL,
  `kemantapan` int(11) NOT NULL,
  `keserasian` int(11) NOT NULL,
  `waktu` int(10) NOT NULL,
  `skor` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_ganda`
--

INSERT INTO `form_ganda` (`juri`, `senjata_lepas`, `pakaian`, `suara`, `garis`, `serang_bela`, `kemantapan`, `keserasian`, `waktu`, `skor`) VALUES
(1, 0, 0, 0, 0, 50, 50, 50, 0, 150),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_regu`
--

CREATE TABLE IF NOT EXISTS `form_regu` (
  `juri` int(1) NOT NULL,
  `diskualifikasi` int(2) NOT NULL,
  `nilai_penambah` int(2) NOT NULL,
  `pakaian` int(3) NOT NULL,
  `suara` int(3) NOT NULL,
  `garis` int(3) NOT NULL,
  `gerakan` int(3) NOT NULL,
  `waktu` int(4) NOT NULL,
  `skor` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_regu`
--

INSERT INTO `form_regu` (`juri`, `diskualifikasi`, `nilai_penambah`, `pakaian`, `suara`, `garis`, `gerakan`, `waktu`, `skor`) VALUES
(1, 0, 50, 0, 0, 0, 0, 0, 150),
(2, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 0, 0, 0, -5, -1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_tunggal`
--

CREATE TABLE IF NOT EXISTS `form_tunggal` (
  `juri` int(1) NOT NULL,
  `diskualifikasi` int(2) NOT NULL,
  `nilai_penambah` int(2) NOT NULL,
  `pakaian` int(3) NOT NULL,
  `senjata_lepas` int(3) NOT NULL,
  `suara` int(3) NOT NULL,
  `garis` int(3) NOT NULL,
  `gerakan` int(3) NOT NULL,
  `waktu` int(4) NOT NULL,
  `skor` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_tunggal`
--

INSERT INTO `form_tunggal` (`juri`, `diskualifikasi`, `nilai_penambah`, `pakaian`, `senjata_lepas`, `suara`, `garis`, `gerakan`, `waktu`, `skor`) VALUES
(1, 0, 50, 0, 0, 0, 0, 0, 0, 150),
(2, 0, 0, 0, 0, 0, -5, 0, 0, 145),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `juri`
--

CREATE TABLE IF NOT EXISTS `juri` (
  `juri` int(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `babak` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `juri`
--

INSERT INTO `juri` (`juri`, `status`, `babak`) VALUES
(1, 'on', 'II'),
(2, 'off', 'free'),
(3, 'off', 'free'),
(4, 'off', 'free'),
(5, 'off', 'free');

-- --------------------------------------------------------

--
-- Table structure for table `partai`
--

CREATE TABLE IF NOT EXISTS `partai` (
  `id` int(1) NOT NULL,
  `merahI` longtext NOT NULL,
  `merahII` longtext NOT NULL,
  `merahIII` longtext NOT NULL,
  `merahtot` int(11) NOT NULL,
  `biruI` longtext NOT NULL,
  `biruII` longtext NOT NULL,
  `biruIII` longtext NOT NULL,
  `birutot` int(4) NOT NULL,
  `totmerahI` int(10) NOT NULL,
  `totmerahII` int(10) NOT NULL,
  `totmerahIII` int(10) NOT NULL,
  `totbiruI` int(10) NOT NULL,
  `totbiruII` int(10) NOT NULL,
  `totbiruIII` int(10) NOT NULL,
  `pemenang` varchar(10) NOT NULL,
  `menang_karena` varchar(50) NOT NULL,
  `nilai_hukuman1m` longtext NOT NULL,
  `nilai_hukuman2m` longtext NOT NULL,
  `nilai_hukuman3m` longtext NOT NULL,
  `total_hukumanm` int(5) NOT NULL,
  `nilai_hukuman1b` longtext NOT NULL,
  `nilai_hukuman2b` longtext NOT NULL,
  `nilai_hukuman3b` longtext NOT NULL,
  `total_hukumanb` int(10) NOT NULL,
  `nilai_plus1m` longtext NOT NULL,
  `nilai_plus2m` longtext NOT NULL,
  `nilai_plus3m` longtext NOT NULL,
  `total_plusm` int(5) NOT NULL,
  `nilai_plus1b` longtext NOT NULL,
  `nilai_plus2b` longtext NOT NULL,
  `nilai_plus3b` longtext NOT NULL,
  `total_plusb` int(5) NOT NULL,
  `nilai_plusA1m` longtext NOT NULL,
  `nilai_plusA2m` longtext NOT NULL,
  `nilai_plusA3m` longtext NOT NULL,
  `nilai_plusA1b` longtext NOT NULL,
  `nilai_plusA2b` longtext NOT NULL,
  `nilai_plusA3b` longtext NOT NULL,
  `nilai_plusB1m` longtext NOT NULL,
  `nilai_plusB2m` longtext NOT NULL,
  `nilai_plusB3m` longtext NOT NULL,
  `nilai_plusB1b` longtext NOT NULL,
  `nilai_plusB2b` longtext NOT NULL,
  `nilai_plusB3b` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partai`
--

INSERT INTO `partai` (`id`, `merahI`, `merahII`, `merahIII`, `merahtot`, `biruI`, `biruII`, `biruIII`, `birutot`, `totmerahI`, `totmerahII`, `totmerahIII`, `totbiruI`, `totbiruII`, `totbiruIII`, `pemenang`, `menang_karena`, `nilai_hukuman1m`, `nilai_hukuman2m`, `nilai_hukuman3m`, `total_hukumanm`, `nilai_hukuman1b`, `nilai_hukuman2b`, `nilai_hukuman3b`, `total_hukumanb`, `nilai_plus1m`, `nilai_plus2m`, `nilai_plus3m`, `total_plusm`, `nilai_plus1b`, `nilai_plus2b`, `nilai_plus3b`, `total_plusb`, `nilai_plusA1m`, `nilai_plusA2m`, `nilai_plusA3m`, `nilai_plusA1b`, `nilai_plusA2b`, `nilai_plusA3b`, `nilai_plusB1m`, `nilai_plusB2m`, `nilai_plusB3m`, `nilai_plusB1b`, `nilai_plusB2b`, `nilai_plusB3b`) VALUES
(1, '1,   1,   2,   3,   2,   1+3, ', '', '', 13, '2,   1+1, ', '', '', 4, 13, 0, 0, 4, 0, 0, ' ', 'mabuk', '', '', '', 0, '', '', '', 0, '1+3, ', '', '', 4, '1+1, ', '', '', 2, '1,   1,   2,   2,   ', '', '', '2,   1+1, ', '', '', '3,   1+3, ', '', '', '', '', ''),
(2, ' ', ' ', ' ', 0, ' ', ' ', '3,   2,   ', 0, 0, 0, 0, 0, 0, 0, ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(3, ' ', ' ', ' ', 0, ' ', ' ', '-1,  1+1, 1,   2,   ', 0, 0, 0, 0, 0, 0, 0, ' ', 'kekenyangan', ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(4, ' ', ' ', ' ', 0, ' ', ' ', '1+2, 1+1, ', 0, 0, 0, 0, 0, 0, 0, ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(5, ' ', ' ', ' ', 0, ' ', ' ', '2,   ', 0, 0, 0, 0, 0, 0, 0, ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', 0, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `peserta_ganda`
--

CREATE TABLE IF NOT EXISTS `peserta_ganda` (
`id` int(4) NOT NULL,
  `nama1` varchar(30) NOT NULL,
  `nama2` varchar(30) NOT NULL,
  `kontingen` varchar(30) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `nomor_urut` int(2) NOT NULL,
  `pool` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `skor` int(4) NOT NULL,
  `waktu` varchar(6) NOT NULL,
  `kebenaran` int(3) NOT NULL,
  `kemantapan` int(3) NOT NULL,
  `penghayatan` int(3) NOT NULL,
  `hukuman` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta_ganda`
--

INSERT INTO `peserta_ganda` (`id`, `nama1`, `nama2`, `kontingen`, `deskripsi`, `nomor_urut`, `pool`, `status`, `skor`, `waktu`, `kebenaran`, `kemantapan`, `penghayatan`, `hukuman`) VALUES
(2, 'jojon', 'hose', 'Jakarta', 'sdeasaaaaszz', 2, 'A', 'sudah', 378, '0:15', 0, 0, 0, 0),
(4, 'sd', 'as', 'gert', 'senjata jatuh 2x, jurus terahir leher di gorok', 4, 'A', 'ya', 0, '', 0, 0, 0, 0),
(5, 'we', 'dw', 'desss', 'vdfv dfvd dvd ', 5, 'final', 'belum', 0, '', 0, 0, 0, 0),
(7, 'Tono', 'Budi', 'Bandung', 'dssad', 2, 'A', 'Belum', 0, '-', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `peserta_regu`
--

CREATE TABLE IF NOT EXISTS `peserta_regu` (
`id` int(4) NOT NULL,
  `nama1` varchar(30) NOT NULL,
  `nama2` varchar(30) NOT NULL,
  `nama3` varchar(30) NOT NULL,
  `kontingen` varchar(30) NOT NULL,
  `nomor_urut` int(2) NOT NULL,
  `pool` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `skor` int(4) NOT NULL,
  `waktu` varchar(6) NOT NULL,
  `kebenaran` int(3) NOT NULL,
  `kompak` int(3) NOT NULL,
  `hukuman` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta_regu`
--

INSERT INTO `peserta_regu` (`id`, `nama1`, `nama2`, `nama3`, `kontingen`, `nomor_urut`, `pool`, `status`, `skor`, `waktu`, `kebenaran`, `kompak`, `hukuman`) VALUES
(2, 'Bima', 'Ari', 'Sofyan', 'Sungai', 1, 'A', 'ya', 0, '', 0, 0, 0),
(3, 'Bima', 'Ari', 'Sofyan', 'Sungai', 2, 'A', 'belum', 0, '', 0, 0, 0),
(5, 'Tono', 'Yono', 'Bowo', 'Banyumas', 1, 'A', 'Belum', 0, '-', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `peserta_tunggal`
--

CREATE TABLE IF NOT EXISTS `peserta_tunggal` (
`id` int(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kontingen` varchar(30) NOT NULL,
  `nomor_urut` int(2) NOT NULL,
  `pool` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `waktu` varchar(6) NOT NULL,
  `skor` int(4) NOT NULL,
  `kebenaran` int(4) NOT NULL,
  `kemantapan` int(4) NOT NULL,
  `hukuman` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta_tunggal`
--

INSERT INTO `peserta_tunggal` (`id`, `nama`, `kontingen`, `nomor_urut`, `pool`, `status`, `waktu`, `skor`, `kebenaran`, `kemantapan`, `hukuman`) VALUES
(6, 'dsd', 'adsa', 4, 'A', 'sudah', '0:10', 357, 300, 0, 0),
(7, 'bima', 'upn', 2, 'A', 'sudah', '3:00', 436, 0, 0, 0),
(8, 'erwan', 'up', 1, 'A', 'sudah', '3:00', 447, -1, -1, -5),
(9, 'irfan', 'upn', 3, 'A', 'sudah', '2:51', 368, 0, 0, 0),
(10, 'riski', 'upn', 5, 'A', 'sudah', '2:48', 449, 286, 165, 0),
(13, 'Wijaya', 'Lampung', 2, 'A', 'Belum', '-', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pesilat`
--

CREATE TABLE IF NOT EXISTS `pesilat` (
  `partai` int(10) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `namamerah` varchar(30) NOT NULL,
  `konmerah` varchar(30) NOT NULL,
  `namabiru` varchar(30) NOT NULL,
  `konbiru` varchar(30) NOT NULL,
  `Bertanding` varchar(7) NOT NULL,
  `pemenang` varchar(10) NOT NULL,
  `skormerah` varchar(2) NOT NULL,
  `skorbiru` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesilat`
--

INSERT INTO `pesilat` (`partai`, `kelas`, `namamerah`, `konmerah`, `namabiru`, `konbiru`, `Bertanding`, `pemenang`, `skormerah`, `skorbiru`) VALUES
(1, 'A Putra', 'Bima Pandu Wiguna', 'upn', 'Pandu Wibowo', 'upn', 'sudah', 'Merah', '1', '0'),
(2, 'A Putra', 'erik', 'upn', 'aji', 'ugm', 'sudah', 'Merah', '4', '1'),
(3, 'B Putri', 'yni', 'upn', 'ijah', 'uii', 'sudah', '', '', ''),
(4, 'B Putri', 'ririn', 'ugm', 'retno', 'uajy', 'Belum', '----------', '0', '0'),
(122, 'A Putra', 'Zaenal', 'Bekasi', 'Topik', 'Bogor', 'Belum', '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `pool`
--

CREATE TABLE IF NOT EXISTS `pool` (
`id` int(3) NOT NULL,
  `pool` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pool`
--

INSERT INTO `pool` (`id`, `pool`) VALUES
(6, 'final'),
(9, 'A'),
(10, 'B');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `form_ganda`
--
ALTER TABLE `form_ganda`
 ADD PRIMARY KEY (`juri`);

--
-- Indexes for table `form_regu`
--
ALTER TABLE `form_regu`
 ADD PRIMARY KEY (`juri`);

--
-- Indexes for table `form_tunggal`
--
ALTER TABLE `form_tunggal`
 ADD PRIMARY KEY (`juri`);

--
-- Indexes for table `juri`
--
ALTER TABLE `juri`
 ADD PRIMARY KEY (`juri`);

--
-- Indexes for table `partai`
--
ALTER TABLE `partai`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_ganda`
--
ALTER TABLE `peserta_ganda`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_regu`
--
ALTER TABLE `peserta_regu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_tunggal`
--
ALTER TABLE `peserta_tunggal`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesilat`
--
ALTER TABLE `pesilat`
 ADD PRIMARY KEY (`partai`);

--
-- Indexes for table `pool`
--
ALTER TABLE `pool`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `peserta_ganda`
--
ALTER TABLE `peserta_ganda`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `peserta_regu`
--
ALTER TABLE `peserta_regu`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `peserta_tunggal`
--
ALTER TABLE `peserta_tunggal`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pool`
--
ALTER TABLE `pool`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
